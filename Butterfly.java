public class Butterfly{
	// Fields
	private String colour;
	private int ageInMinutes;
	private boolean onFlower;
	
	// Constructor
	public Butterfly(String colour, int ageInMinutes, boolean onFlower) {
		this.colour = colour;
		this.ageInMinutes = ageInMinutes;
		this.onFlower = onFlower;
	}
	
	// Setters
	public void setColour(String newColour) {
		this.colour = newColour;
	}
	
	// Getters
	public String getColour() {
		return this.colour;
	}
	
	public int getAgeInMinutes() {
		return this.ageInMinutes;
	}
	
	public boolean getIsOnFlower() {
		return this.onFlower;
	}
	
	public void canFly(){
		if(this.ageInMinutes < 0){
			System.out.println("Your butterfly hasn't hatched yet, be patient.");
		}
		else if(this.ageInMinutes == 0){
			System.out.println("Your butterfly is hatching, go watch it.");
		}
		else if(this.ageInMinutes <= 90){
			System.out.println("It is learning how to fly, wait a little.");
		}
		else{
			System.out.println("It can fly now!");
		}
	}
	public void canPollinate(){
		if(this.onFlower){
			System.out.println("It is pollinating.");
		}
		else{
			System.out.println("It isn't pollinating.");
		}
	}
}
	