import java.util.Scanner;

public class VirtualPetApp{
	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Butterfly[] kaleidoscope = new Butterfly[1];
		
		for(int i=0; i<kaleidoscope.length; i++){
			System.out.println("Enter the colour of your butterfly.");
			String colour = reader.nextLine();
			System.out.println("How old in minutes is your butterfly.");
			int ageInMinutes = Integer.parseInt(reader.nextLine());
			System.out.println("Is your butterfly on a flower, true for yes or false for no.");
			boolean onFlower = Boolean.parseBoolean(reader.nextLine());

			kaleidoscope[i] = new Butterfly(colour, ageInMinutes, onFlower);
		}
		
		System.out.println("Enter a new colour:");
		String newColour = reader.nextLine();
		kaleidoscope[kaleidoscope.length - 1].setColour(newColour);
		
		System.out.println(kaleidoscope[kaleidoscope.length - 1].getColour());
		System.out.println(kaleidoscope[kaleidoscope.length - 1].getAgeInMinutes());
		System.out.println(kaleidoscope[kaleidoscope.length - 1].getIsOnFlower());
	}
}